import { Department } from '../department';

export class Employee{
    
	 id : Number;
	 empId : String;
	 empFirstName : String;
	 empMidleName : String;
	 emplLastName : String;
	 empGender : String;
	 empType : String;
	 isDeleted : Boolean;
	 department :Department;
	 employeeImage:File[];

}