export class Address{
    id:Number;
	countryId : String;
	countryName : String;
	cityName : String;
	stateName : String;
	zepCode : Number;
	isDeleted : Boolean;
}