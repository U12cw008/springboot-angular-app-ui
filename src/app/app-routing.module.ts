import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartmentListComponent } from './component/DepartmentComponent/ListView/department/departmentComponent';
import { DepartmentCompnentAdd } from './component/DepartmentComponent/addOrEdit/department/departmentComponent';
import { EmployeeComponentAdd } from './component/DepartmentComponent/addOrEdit/employee/employeeComponent';
import { EmployeeComponent } from './component/DepartmentComponent/ListView/employee/employeeComponent';
import { AddressComponent } from './component/DepartmentComponent/ListView/address/addressComponent';
import { AddressComponentAdd } from './component/DepartmentComponent/addOrEdit/address/addressComponent';
import { BenefitComponentComponent } from './component/DepartmentComponent/ListView/benefit/benefit-component/benefit-component.component';
import { BenefitComponentAddComponent } from './component/DepartmentComponent/addOrEdit/benefit/benefit-component-add/benefit-component-add.component';
import { DeductionComponent } from './component/DepartmentComponent/ListView/deduction/deduction/deduction.component';
import { DeductionAddComponent } from './component/DepartmentComponent/addOrEdit/deduction/deduction-add/deduction-add.component';


const routes: Routes = [

  { path: 'department', component: DepartmentListComponent },
  { path: 'employee', component:  EmployeeComponent },
  { path: 'address', component:  AddressComponent },
  { path: 'departmentAdd', component: DepartmentCompnentAdd },
  { path: 'employeeAdd', component:  EmployeeComponentAdd },
  { path: 'addressAdd', component:  AddressComponentAdd },
  { path: 'benefit', component:  BenefitComponentComponent },
  { path: 'benefitAdd', component:  BenefitComponentAddComponent },
  { path: 'deduction', component:  DeductionComponent },
  { path: 'deductionAdd', component: DeductionAddComponent
}
  
  

  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
