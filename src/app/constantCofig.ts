export class ConstantConfig{

    static baseUre_ForDepartment = 'http://localhost:8080/departmentController/';
    static baseUrl_ForEmployee = 'http://localhost:8080/employeeController/';
    static baseUrl_ForAddress = 'http://localhost:8080/addressController/';
    static baseUrl_ForBenefit = 'http://localhost:8080/benefitCodeController/';
    static baseurl_ForDeduction = 'http://localhost:8080/deductionCodeController/';

}