import { Component, OnInit, ViewChild } from "@angular/core";
import { AddressService } from '../.../../../../../service/AddressService/AddressService';
import { Router } from '@angular/router';
import { Address } from '../../../../model/address/address';
import { NgxPaginationModule } from 'ngx-pagination';



@Component({
    selector:'app-component',
    templateUrl:'/addressComponent.html',
    styleUrls:['/addressComponent.css']
})
export class AddressComponent implements OnInit{
    addresses =[];

    sortColumn = 'stateName';
    sortDirection = 'asc';
    pageSize = 5;
    filterBy = {'searchKey' : '' };

    constructor(private addressService:AddressService,private router:Router){}



    ngOnInit() {
        this.getAllAddress();
    }
    getAllAddress(){
       return this.addressService.getAllAddress().subscribe(data=>{
            this.addresses=data;
        });

    }
    deleteAddress(address){
        this.addressService.deleteAddress(address).subscribe(data=>{
            this.addresses.splice(this.addresses.indexOf(address),1);
        });
    }

    saveOrUpdate(address){
        this.addressService.setter(address);
        this.router.navigate(['/addressAdd']);
    }


searchAddressData(filterBy){
   this.addressService.searchAddressData(`${this.sortColumn},${this.sortDirection}`,0,this.pageSize,filterBy)
   .subscribe(
       response => {
           this.addresses = response.content;
       }
   );
 }

 onfilter(event){
     this.filterBy.searchKey = event.target.value;
     this.searchAddressData(this.filterBy);
 }

    newAddress(){
        let address =new Address();
        this.addressService.setter(address);
        this.router.navigate(['/addressAdd']);
    }
}