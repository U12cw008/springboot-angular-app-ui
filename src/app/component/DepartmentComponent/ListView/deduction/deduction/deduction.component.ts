import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeductionService } from 'src/app/service/DeductionService/DeductionService';
import { Deduction } from 'src/app/model/deduction/deduction';

@Component({
  selector: 'app-deduction',
  templateUrl: './deduction.component.html',
  styleUrls: ['./deduction.component.css']
})
export class DeductionComponent implements OnInit {

  deductions :any;
  id = {'id' : ''};

  sortColumn = 'deductionName';
  sortDirection ='asc';
  pageSize =5;
  filterBy = {'searchKey' : ''};
  constructor(private router :Router, private deductionService :DeductionService) { }
  
  ngOnInit() {
    this.getAllDeduction();
  }

  getAllDeduction(){
    return this.deductionService.getAllDeductionCode().subscribe((data)=>{
      this.deductions =data;
    });
  }

  deleteDeductionCode(deduction){
    this.deductionService.deleteeduction(deduction).subscribe((data)=>{
      this.deductions.splice(this.deductions.indexOf(deduction),1);
    });
  }

  searchDeductionData(filterBy){
    this.deductionService.searchDeduction(`${this.sortColumn},${this.sortDirection}`,0,this.pageSize,filterBy)
    .subscribe(
        response =>{
            this.deductions = response.content;
        }
    );
  }
  onFilter(event){
    this.filterBy.searchKey = event.target.value;
    this.searchDeductionData(this.filterBy);
  }
  saveOrUpdate(id){
    this.deductionService.setter(id);
    this.router.navigate(['/deductionAdd']);
  }
  addNemItem(){
    let deduction = new Deduction();
    this.deductionService.setter(deduction);
    this.router.navigate(['/deductionAdd']);
  }
}
