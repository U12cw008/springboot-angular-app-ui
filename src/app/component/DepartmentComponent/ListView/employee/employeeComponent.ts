import { Component, OnInit } from "@angular/core";
import { EmployeeService } from '../../../../service/EmployeeService/employeeService';
import { Router } from '@angular/router';
import { Employee } from '../../../../model/employee/employee';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

@Component({
    selector: 'app-component',
    templateUrl: './employeeComponent.html',
    styleUrls: [ './employeeComponent.css' ]
})
export class EmployeeComponent implements OnInit{

    sortColumn = 'empFirstName';
    sortDirection ='asc';
    pageSize =5;
    filterBy = {'searchKey' : ''};

    employees:any;
    formula:string = "Employee csv";
    constructor(private employeeService:EmployeeService,private router:Router){}

    ngOnInit() {
       this.getAllEmployee();
    }
    getAllEmployee(){
        this.employeeService.getAllEmployee().subscribe(data=>{
            this.employees=data;
        });
    }
    deleteEmployee(employee){
        this.employeeService.deleteEmployee(employee).subscribe(data=>{
            this.employees.splice(this.employees.indexOf(employee),1);
        });
    }

    saveOrUpdateEmployee(employee){
        this.employeeService.setter(employee);
        this.router.navigate(['/employeeAdd']);
    }
    newEmployee(){
        let employee =new Employee();
        this.employeeService.setter(employee);
        this.router.navigate(['/employeeAdd']);
    }

    searchEmployeeData(filterBy){
        this.employeeService.searchEmployee(`${this.sortColumn},${this.sortDirection}`,0,this.pageSize,filterBy)
        .subscribe(
            response =>{
                this.employees = response.content;
            }
        );
    }
    onFilter(event){
        this.filterBy.searchKey = event.target.value;
        this.searchEmployeeData(this.filterBy);
    }

    downloadCSVForEmployee(){
        this.employeeService.downloadCSV().subscribe((data) =>{
            this.employees = data;
        });
        new Angular2Csv(this.employees, this.formula);
    }


}