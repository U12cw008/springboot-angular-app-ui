import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitComponentComponent } from './benefit-component.component';

describe('BenefitComponentComponent', () => {
  let component: BenefitComponentComponent;
  let fixture: ComponentFixture<BenefitComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
