import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BenefitService } from '../../../../../service/BenefitService/BenefitService';
import { Benefit } from 'src/app/model/Benefit/Benefit';

@Component({
  selector: 'app-benefit-component',
  templateUrl: './benefit-component.component.html',
  styleUrls: ['./benefit-component.component.css']
})
export class BenefitComponentComponent implements OnInit {

  benefits : any;
  sortColumn = 'benefitCode';
  sortDirection = 'asc';
  pageSize = 5;
  filterBy = {'searchKey' : ''} 
  constructor(private router : Router ,private benefitService : BenefitService ) { }

  ngOnInit() {
    this.getBenefit();

  }

  getBenefit(){
    return this.benefitService.getAllBenefit().subscribe(data=>{
      this.benefits = data;
    })
  }

  saveOrUpdate(benefit){
    this.benefitService.setter(benefit);
    this.router.navigate(['/benefitAdd']);
  }
  addNemItem(){
    let benefit = new Benefit();
    this.benefitService.setter(benefit);
    this.router.navigate(['/benefitAdd']);
  }

  searchBenefitData(filterBy){
     this.benefitService.searchBenefit(`${this.sortColumn},${this.sortDirection}`,0,this.pageSize,filterBy)
     .subscribe(
       response =>{
         this.benefits = response.content;
       }
     );
  }

  onFilter(event){
    this.filterBy.searchKey = event.target.value;
    this.searchBenefitData(this.filterBy);

  }

}
