import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { Department } from 'src/app/model/department';
import { DepartmentService } from '../../../../service/DepartmentService/departmentService';

@Component({
    selector: 'app-component',
    templateUrl: './departmentComponent.html',
    styleUrls: [ './departmentComponent.css' ]
})
export class DepartmentListComponent implements OnInit{

    departments:any;

    sortColumn = 'departmentName';
    sortDirection ='asc';
    pageSize =5;
    filterBy = {'searchKey' : ''};

    constructor(private departmentService:DepartmentService,private router:Router){}

    ngOnInit() {
        this.getAllDepartment();
        
    }

    getAllDepartment(){
        return this.departmentService.getAllDepartment().subscribe(data=>{
            this.departments=data;
        })
    }

    deletetDepartment(department){
         this.departmentService.deleteDepartment(department).subscribe(data=>{
            this.departments.splice(this.departments.indexOf(department),1);
        });
    }

    saveOrUpdate(departmet){  
        this.departmentService.setter(departmet);
        this.router.navigate(['/departmentAdd']);
      }
   
      newItem(){
      let department = new Department();
       this.departmentService.setter(department);
        this.router.navigate(['/departmentAdd']);
      }

      searchDepartmentData(filterBy){
          this.departmentService.searchDepartment(`${this.sortColumn},${this.sortDirection}`,0,this.pageSize,filterBy)
          .subscribe(
              response =>{
                  this.departments = response.content;
              }
          );

      }

      onFilter(event){
          this.filterBy.searchKey = event.target.value;
          this.searchDepartmentData(this.filterBy);
      }


}