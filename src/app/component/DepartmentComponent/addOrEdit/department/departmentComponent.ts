import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Department } from '../../../../model/department';
import { DepartmentService } from '../../../../service/DepartmentService/departmentService';



@Component({
    selector: 'app-component',
    templateUrl: './departmentComponent.html',
    styleUrls: [ './departmentComponent.css' ]
})
export class DepartmentCompnentAdd implements OnInit{
    departmentForm: FormGroup;
    submitted = false;
    department : Department=new Department();
    constructor(private departmentService:DepartmentService,private router:Router,private formBuilder: FormBuilder){}


    
    ngOnInit(){
        this.department=this.departmentService.getter();
        this.formValidator();
    }
formValidator(){
    this.departmentForm = this.formBuilder.group({
        depeartmentId: ['', Validators.required],
        departmentName: ['', Validators.required],
    });
}
get f() {
     return this.departmentForm.controls; 
    }


    processForm(){
        this.submitted = true;
        if (this.departmentForm.invalid) {
            return;
        }
        if(this.department.id==undefined){
            this.departmentService.createDepartment(this.department).subscribe((department)=>{
                alert("department created successfully.")
                this.router.navigate(['/department'])
            })
        }else{
            this.departmentService.updateDepartment(this.department).subscribe((department)=>{
                alert("department updated successfully.")
                this.router.navigate(['/department'])
            })
        }
    }
    
    cancel(){
        this.router.navigate(['/department'])
    }
    
}
