import { Component, OnInit } from '@angular/core';
import { Deduction } from 'src/app/model/deduction/deduction';
import { Router } from '@angular/router';
import { DeductionService } from 'src/app/service/DeductionService/DeductionService';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-deduction-add',
  templateUrl: './deduction-add.component.html',
  styleUrls: ['./deduction-add.component.css']
})
export class DeductionAddComponent implements OnInit {

  deduction :Deduction =new Deduction();
  submitted= false;
  deductionForm :FormGroup;
  constructor(private router :Router , private deductionService :DeductionService , private formBuilder :FormBuilder) { }

  ngOnInit() {
    this.deduction =this.deductionService.getter();
    this.formValidator();
  }

formValidator(){
  this.deductionForm = this.formBuilder.group({
    deductionId: ['', Validators.required],
    deductionName: ['', Validators.required],
    deductionCode: ['', Validators.required],
    deductionDesc: ['', Validators.required]
  });
}

get f() {
  return this.deductionForm.controls; 
 }

 processForm(){
   this.submitted = true;
   if(this.deductionForm.invalid){
     return true;
   }
     if(this.deduction.id == undefined){
       this.deductionService.createDeduction(this.deduction).subscribe((data)=>{
         this.router.navigate(['/deduction']);
       });
     }else{
       this.deductionService.updateDeduction(this.deduction).subscribe((data)=>{
         this.router.navigate(['/deduction']);
       });
     }
   }

   
  reset(){
    this.router.navigate(['/deduction'])
  }
}
