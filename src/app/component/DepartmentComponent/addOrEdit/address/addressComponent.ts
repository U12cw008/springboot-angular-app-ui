import { Component, OnInit } from "@angular/core";
import { Address } from '../../../../model/address/address';
import { Router } from '@angular/router';
import { AddressService } from '../../../../service/AddressService/AddressService';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector:'app-component',
    templateUrl:'./addressComponent.html',
    styleUrls:['./addressComponent.css'],
})

export class AddressComponentAdd implements OnInit{
        addressForm:FormGroup;
        submitted = false;
        address : Address =new Address();
        constructor (private  addressService:AddressService ,private router:Router,private formBuilder:FormBuilder){}
        

    ngOnInit(){
        this.address=this.addressService.getter();
        this.formValidator();
    }

    formValidator(){
        this.addressForm = this.formBuilder.group({
            countryId : ['', Validators.required],
            countryName : ['', Validators.required],
            cityName : ['', Validators.required],
            stateName : ['', Validators.required],
            zepCode : ['', Validators.required],
        });
    }
    get f(){
        return this.addressForm.controls;
    }

    processForm(){
        this.submitted = true;
        if(this.addressForm.invalid){
            return;
        }

        if(this.address.id == undefined){
            this.addressService.createAddress(this.address).subscribe(data=>{
                this.router.navigate(['/address']);
            });
        }else{
            this.addressService.updateAddress(this.address).subscribe(data=>{
                this.router.navigate(['/address']);
            });
        }
    }

    cancel(){
        this.router.navigate(['/address']);
    }
}