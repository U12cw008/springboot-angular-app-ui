import { Component, OnInit } from "@angular/core";
import { Employee } from '../../../../model/employee/employee';
import { EmployeeService } from '../../../.../../../service/EmployeeService/employeeService';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentService } from '../../../../service/commonService/DocumentService';
import {map} from 'rxjs/operators';


@Component({
    selector: 'app-component',
    templateUrl: './employeeComponent.html',
    styleUrls: [ './employeeComponent.css' ]
})
export class EmployeeComponentAdd implements OnInit{

    employee :Employee = new Employee();
    employees:any;
    employeeForm : FormGroup;
    submitted = false;
    // file properties
    file:File;
    isSelected: boolean = false;
    fileSelect: any;
    fileName: string;
    url = '';


    constructor(private employeeService:EmployeeService,private documentService: DocumentService,private router:Router,private formBuilder: FormBuilder){}

    ngOnInit(){
        this.employee=this.employeeService.getter();
        this.formValidator();
    }

    formValidator(){
        this.employeeForm = this.formBuilder.group({
            empId: ['', Validators.required],
            empFirstName: ['', Validators.required],
            empMidleName:['',Validators.required],
            emplLastName:['',Validators.required],
            empGender : ['',Validators.required],
            empType :['', Validators.required]


        });
    }
    get f() {
         return this.employeeForm.controls; 
        }

    
    processForm(){
        this.submitted = true;
        if (this.employeeForm.invalid) {
            return;
        }
            if(this.employee.id == undefined){
                this.employeeService.createEmployee(this.employee,this.file).subscribe((data)=>{
                    this.router.navigate(['/employee']);
                });
            }
        
        else{
            this.employeeService.updateEmployee(this.employee,this.file).subscribe((data)=>{
                this.router.navigate(['/employee']);
            });
        }
    }

    exportPdf(){
        this.employeeService.exportEmployeePdf(this.employee.id)
        .pipe(
            map(
                response=>{
                    const blob = new Blob([response.body as any],{type :'application/pdf'});
                    this.documentService.download(blob,this.documentService.getFileName(response.headers));
                }
            )
        ).subscribe(
            response => {},
        )
    }

    cancel(){
        this.router.navigate(['/employee']);
    }





    onUploadBtn(event) {
        //console.log("aaaa:", event.target.files[0]);
        this.fileName = ""

        if (event.target.files && event.target.files[0]) {
            this.fileName = event.target.files[0].name;
            this.fileSelect = event.target.files[0];
            var reader = new FileReader();

            if (
                (this.fileSelect.type == "image/png" ||
                    this.fileSelect.type == "image/jpg" ||
                    this.fileSelect.type == "image/jpeg")
            ) {
                this.isSelected = false;
            } else {
                this.isSelected = false;
                return false;
            }

            if (event.target.files[0].size / 1048576 > 2) {
                return false;
            }

            this.file = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]); 
            reader.onload = (event: any) => { 
                this.url = event.target.result;            }
        }
    }







}

