import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BenefitService } from '../.../../../../../../service/BenefitService/BenefitService';
import { Benefit } from '../../../../../model/Benefit/Benefit';
import { FormGroup ,FormBuilder,Validators} from '@angular/forms';

@Component({
  selector: 'app-benefit-component-add',
  templateUrl: './benefit-component-add.component.html',
  styleUrls: ['./benefit-component-add.component.css']
})
export class BenefitComponentAddComponent implements OnInit {
  benefitForm : FormGroup;
  submitted= false;
  benefit :Benefit= new Benefit();

  constructor(private router : Router,private benefitService: BenefitService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.benefit = this.benefitService.getter();
    this.formValidator();
  }

  
formValidator(){
  this.benefitForm = this.formBuilder.group({
    benefitId: ['', Validators.required],
    benefitCode: ['', Validators.required],
    benefitDesc: ['', Validators.required]
  });
}

get f() {
  return this.benefitForm.controls; 
 }
  processForm(){
    this.submitted = true;
    if(this.benefitForm.invalid){
      return;
    }
    if(this.benefit.id == undefined){
      this.benefitService.createBenefit(this.benefit).subscribe((benefit)=>{
        this.router.navigate(['/benefit']);
      })
    }else{
      this.benefitService.updateBenefit(this.benefit).subscribe((benefit)=>{
        this.router.navigate(['/benefit']);
      })
    }
  }


  reset(){
    this.router.navigate(['/benefit'])
  }

}
