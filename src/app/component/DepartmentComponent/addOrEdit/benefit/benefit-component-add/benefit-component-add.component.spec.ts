import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitComponentAddComponent } from './benefit-component-add.component';

describe('BenefitComponentAddComponent', () => {
  let component: BenefitComponentAddComponent;
  let fixture: ComponentFixture<BenefitComponentAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenefitComponentAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitComponentAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
