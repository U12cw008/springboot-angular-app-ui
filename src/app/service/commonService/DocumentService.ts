import { Injectable } from '@angular/core';
import {HttpHeaders} from '@angular/common/http'

@Injectable({
    providedIn: 'root'
  })
export class DocumentService {

    download(blob: Blob ,Name: String){
        const a = document.createElement('a');
        document.body.appendChild(a);
        const url = window.URL.createObjectURL(blob);
        a.href = url;
        const fileName = name;
        a.download = fileName;
        a.target = '_blank';
        const event = new MouseEvent('click',{
            'view' : window,
            'bubbles' : false,
            'cancelable' : true
        });
        a.dispatchEvent(event);
        setTimeout(() => {
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
        },0);
    }

    getFileName(headers: HttpHeaders){
        return headers.get('X-Doc-Name') || headers.get('x-doc-name') || 'file.csv';
    }

    getContentType(headers: HttpHeaders){
        return headers.get('Content-Type');
    }

}