import { Injectable } from "@angular/core";
import { ConstantConfig } from 'src/app/constantCofig';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Employee } from '../../model/employee/employee';
import { Department } from '../../model/department';
import { Observable } from 'rxjs';


@Injectable({
    providedIn:'root'
})
export class EmployeeService{

    employee :Employee =new Employee();
    private baseUrl_ForCreate=ConstantConfig.baseUrl_ForEmployee +'/create';
    private baseUrl_ForUpdate=ConstantConfig.baseUrl_ForEmployee +'/update'
    private baseUrl_ForDelete=ConstantConfig.baseUrl_ForEmployee +'/delete';
    private baseUrl_ForGetAllDropDown=ConstantConfig.baseUrl_ForEmployee +'/getAllDropDown';
    private baseUrl_ForDepartmentDropDown=ConstantConfig.baseUre_ForDepartment +'/getAllDropDownId';
    private beaseUrl_ForExportPDF=ConstantConfig.baseUrl_ForEmployee +'/exportPdf';
    private baseUrl_ForSearchEmployee = ConstantConfig.baseUrl_ForEmployee +'/searchEmployee';
    private baseUrl_ForEmployeeCSV = ConstantConfig.baseUrl_ForEmployee + '/exportCSV';


    constructor(private httpClient:HttpClient){}

    createEmployee(employee,file){
        let formData: FormData = new FormData(); 
        if(file && file.name){
            formData.append('file', file, file.name);
        } 
        formData.append('EmployeeDto', JSON.stringify(employee));
        return this.httpClient.post<Employee>(this.baseUrl_ForCreate,formData);
    }

    updateEmployee(employee,file){
        let formData: FormData = new FormData(); 
        if(file && file.name){
            formData.append('file', file, file.name);
        } 
        formData.append('EmployeeDto', JSON.stringify(employee));
        return this.httpClient.put<Employee>(this.baseUrl_ForUpdate,formData);
    }

    deleteEmployee(employee){
        return this.httpClient.post<Employee>(this.baseUrl_ForDelete,employee);
    }
    getAllEmployee(){
        return this.httpClient.get<Employee[]>(this.baseUrl_ForGetAllDropDown);
    }


    getAllDepartMentId(){
        return this.httpClient.get<Department[]>(this.baseUrl_ForDepartmentDropDown);
    }

    exportEmployeePdf(id){
        
        const href = `${this.beaseUrl_ForExportPDF}`
        let body = new HttpParams();
        body = body.set('id',id);
        return this.httpClient.post(href,body , {responseType: 'blob' as 'text' ,observe: 'response'});
    }



    searchEmployee(sort :string ,page: number, size: Number, body: any){
        const href= this.baseUrl_ForSearchEmployee;
        return this.httpClient.post<any>(href,body,{
            params: new HttpParams()
            .set('page',page.toString())
            .set('size',size.toString())
            .set('sort',sort.toString())
        });
    }


    downloadCSV(): Observable<any> {
        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/csv');
      
        return this.httpClient.get(this.baseUrl_ForEmployeeCSV, {
          headers: headers,
          responseType: 'text'
        });
      }

    setter(employee:Employee){
        this.employee=employee;
    }
    getter(){
        return this.employee;
    }
    
}