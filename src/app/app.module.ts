import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http' 
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { EmployeeComponent } from './component/DepartmentComponent/ListView/employee/employeeComponent';
import { DepartmentListComponent } from './component/DepartmentComponent/ListView/department/departmentComponent';
import { DepartmentCompnentAdd } from './component/DepartmentComponent/addOrEdit/department/departmentComponent';
import { EmployeeComponentAdd } from './component/DepartmentComponent/addOrEdit/employee/employeeComponent';
import { AddressComponent } from './component/DepartmentComponent/ListView/address/addressComponent';
import { AddressComponentAdd } from './component/DepartmentComponent/addOrEdit/address/addressComponent';
import { BenefitComponentComponent } from './component/DepartmentComponent/ListView/benefit/benefit-component/benefit-component.component';
import { BenefitComponentAddComponent } from './component/DepartmentComponent/addOrEdit/benefit/benefit-component-add/benefit-component-add.component';
import { DeductionComponent } from './component/DepartmentComponent/ListView/deduction/deduction/deduction.component';
import { DeductionAddComponent } from './component/DepartmentComponent/addOrEdit/deduction/deduction-add/deduction-add.component';

@NgModule({
  declarations: [
    AppComponent,
    DepartmentListComponent,
    EmployeeComponent,
    AddressComponent,
    DepartmentCompnentAdd,
    EmployeeComponentAdd,
    AddressComponentAdd,
    BenefitComponentComponent,
    BenefitComponentAddComponent,
    DeductionComponent,
    DeductionAddComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
